import pygame, sys
import os
from pygame.locals import *
from math import floor
import time
import game_tools
import gameplay

pygame.init()  # initialise pygaem
pygame.display.set_caption("HagiaSophia")
clock = pygame.time.Clock()

WINDOW_SIZE = (480, 270)
framerate = 60

flags = pygame.RESIZABLE | pygame.DOUBLEBUF | pygame.SCALED | pygame.HWSURFACE
stdscr = pygame.display.set_mode(WINDOW_SIZE, flags, vsync=1)  # start the window

# temp art
player_image = pygame.image.load(os.path.join("test_assets/player/", "plater_01.png"))
grass_image = pygame.image.load("test_assets/player/grass.png")
dirt_image = pygame.image.load("test_assets/player/dirt.png")
myfont = pygame.font.SysFont("monospace", 12)


def load_map(path):
    f = open(path, "r")
    data = f.read()
    f.close()
    data = data.split("\n")
    game_map = []
    for row in data:
        game_map.append(list(row))

    return game_map


game_map = load_map("test_assets/maps/map_01")

player_location = [50, 50]

moving_right = False
moving_left = False
jump = False

player_y_momentum = 0


player_rect = pygame.Rect(
    player_location[0],
    player_location[1],
    player_image.get_width(),
    player_image.get_height(),
)

is_on_ground = False


def collision_test(rect, tiles):
    hit_list = []
    for tile in tiles:
        if rect.colliderect(tile):
            hit_list.append(tile)
    return hit_list


def move(rect, movement, tiles):
    collision_types = {"top": False, "bottom": False, "right": False, "left": False}
    rect.x += movement[0]
    hit_list = collision_test(rect, tiles)
    for tile in hit_list:
        if movement[0] > 0:
            rect.right = tile.left
            collision_types["right"] = True
        elif movement[0] < 0:
            rect.left = tile.right
            collision_types["left"] = True
    rect.y += movement[1]
    hit_list = collision_test(rect, tiles)
    for tile in hit_list:
        if movement[1] > 0:
            rect.bottom = tile.top
            collision_types["bottom"] = "True"
        if movement[1] < 0:
            rect.top = tile.bottom
            collision_types["top"] = True

    return rect, collision_types


last_time = time.time()

frames = 0
frame_time = time.time()

# finished classes implementation
engine_clock = game_tools.main_clock()
player_camera = game_tools.my_camera(120, 120, WINDOW_SIZE, engine_clock)
keyboard = game_tools.my_input()
renderer = game_tools.main_renderer(player_camera, stdscr)

# debuf tools
label = myfont.render("FPS", 1, (247, 226, 15))

# test classes
player2 = gameplay.player(engine_clock)
player2.x_pos = 80
player2.y_pos = 100
player2.set_image("test_assets/player", "plater_01.png")
renderer.include_item(player2, 2)
player_camera.target = player2

while True:  # game loop

    # debug
    label = myfont.render(str(engine_clock.framerate), 1, (247, 226, 15))

    # update methods:
    engine_clock.delta_time()
    engine_clock.get_framerate()

    stdscr.fill((146, 244, 255))

    stdscr.blit(label, (1, 1))  # debug

    tile_rects = []
    y = 0
    for row in game_map:
        x = 0
        for tile in row:
            if tile == "1":
                stdscr.blit(dirt_image, player_camera.world_to_cam(x * 32, y * 32))
            if tile == "2":
                stdscr.blit(grass_image, player_camera.world_to_cam(x * 32, y * 32))
            if tile != "0":
                tile_rects.append(pygame.Rect(x * 32, y * 32, 32, 32))
            x += 1
        y += 1

    player_location[1] += player_y_momentum

    player_movement = [0, 0]
    if moving_right:
        player_movement[0] += 2 * engine_clock.delta
    elif moving_left:
        player_movement[0] -= 2 * engine_clock.delta
    if jump and is_on_ground:
        is_on_ground = False
        player_y_momentum = -7
        jump = False
    elif jump and not is_on_ground:
        jump = False

    player_movement[1] += player_y_momentum

    if player_y_momentum < 3:
        player_y_momentum += 0.2

    player_rect, collisions = move(player_rect, player_movement, tile_rects)

    if collisions["bottom"]:
        player_y_momentum = 0
        is_on_ground = True
    if collisions["top"]:
        player_y_momentum = 0

    if abs(player_movement[1]) > 1:
        is_on_ground = False

    # print(player_movement)
    stdscr.blit(player_image, player_camera.world_to_cam(player_rect.x, player_rect.y))
    keyboard.read_input()
    if keyboard.pressed_keys["right"] and not keyboard.pressed_keys["left"]:
        moving_right = True
    else:
        moving_right = False
    if keyboard.pressed_keys["left"] and not keyboard.pressed_keys["right"]:
        moving_left = True
    else:
        moving_left = False
    if keyboard.pressed_keys["up"]:
        jump = True
    else:
        jump = False

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    # print(player_movement)
    renderer.render_to_screen()
    engine_clock.update_state()

    pygame.display.update()  # move to renderer class later
    clock.tick(framerate)
    keyboard.read_input()
