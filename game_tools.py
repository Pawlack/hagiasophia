import pygame, sys
from pygame.locals import *
from math import floor
import time


class my_input:
    """Input class. Keeps track for the state of all input
    including controllers and mouse (yet to be implemented)"""

    def __init__(self):
        self.pressed_keys = {
            "up": False,
            "down": False,
            "left": False,
            "right": False,
            "action_0": False,
        }

    def read_input(self):
        # print(self.pressed_keys)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_RIGHT or event.key == K_d:
                    self.pressed_keys["right"] = True
                if event.key == K_LEFT or event.key == K_a:
                    self.pressed_keys["left"] = True
                if event.key == K_UP or event.key == K_w:
                    self.pressed_keys["up"] = True
                if event.key == K_DOWN or event.key == K_s:
                    self.pressed_keys["down"] = True
                if event.key == K_q:
                    pygame.quit()
                    sys.exit()
                if event.key == K_f:
                    pygame.display.toggle_fullscreen()
            if event.type == KEYUP:
                if event.key == K_RIGHT or event.key == K_d:
                    self.pressed_keys["right"] = False
                if event.key == K_LEFT or event.key == K_a:
                    self.pressed_keys["left"] = False
                if event.key == K_UP or event.key == K_w:
                    self.pressed_keys["up"] = False
                if event.key == K_DOWN or event.key == K_s:
                    self.pressed_keys["down"] = False


class main_clock:
    """main game clock"""

    def __init__(self):
        self.last_time = time.time()
        self.frame_time = time.time()
        self.frames = 0
        self.framerate = 60
        self.frame_time = time.time()
        self.delta = 1
        self.updatable_objects = []  # ticker objects only

    def delta_time(self):
        """Count delta time.
        Should be done on beginning of every frame"""
        t_delta = time.time() - self.last_time
        t_delta *= 60
        self.last_time = time.time()
        self.delta = t_delta

    def get_framerate(self):
        """Count frames. Useful for debug. Not obligatory"""
        self.frames += 1
        if time.time() - self.frame_time >= 1.0:
            # print(frames)
            # label = myfont.render(str(frames), 1, (247,226,15))
            self.framerate = self.frames
            self.frames = 0
            self.frame_time = time.time()

    def update_state(self):
        """update all updatable objects"""
        for object in self.updatable_objects:
            object.tick(self.delta)


class ticker:
    """Objects of this class have require delta-time information"""

    def __init__(self, main_clock: main_clock) -> None:
        main_clock.updatable_objects.append(self)

    def tick(self, delta_time):
        pass


class my_camera(ticker):
    """Basic camera class."""

    def __init__(self, x, y, WINDOW_SIZE: tuple, main_clock: main_clock) -> None:
        super().__init__(main_clock)
        self.x_pos = x
        self.y_pos = y
        self.screen_size = WINDOW_SIZE
        self.target = None
        self.smooth = False
        self.smoothnes = 0

    def move_to(self, x, y):
        self.x_pos = x
        self.y_pos = y

    def follow_target(self):
        if self.target is not None:
            self.move_to(self.target.x_pos, self.target.y_pos)

    def tick(self, delta_time):

        self.follow_target()

    def world_to_cam(self, x, y):
        cam_x = x - (self.x_pos - self.screen_size[0] / 2)
        cam_y = y - (self.y_pos - self.screen_size[1] / 2)
        return cam_x, cam_y

    def toggle_fullscreen(self):
        pygame.display.toggle_fullscreen()


class main_renderer:
    """main rendering class"""

    def __init__(self, camera: my_camera, main_window: pygame.display):
        self.render_queue = {0: [], 1: [], 2: []}  # enough for now
        self.camera = camera
        self.screen = main_window

    def render_to_screen(self):
        # render background color

        for i in range(len(self.render_queue)):
            for item in self.render_queue[i]:
                item.render_to_screen(self.screen, self.camera)

    def include_item(self, item, pass_no):
        self.render_queue[pass_no].append(item)

    def exclude_item(self, item, pass_no):
        # now finding quickly item toremovve from render list is gonna be tricky
        # maybe I should store index of each item somewhere to just use pop
        self.render_queue[pass_no].remove(item)
