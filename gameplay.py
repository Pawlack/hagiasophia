import pygame, sys
import os
from pygame.locals import *
from math import floor
import time
import game_tools


class player(game_tools.ticker):
    def __init__(self, main_clock: game_tools.main_clock) -> None:
        super().__init__(main_clock)
        self.x_pos = 0
        self.y_pos = 0
        self.image = None
        self.rect = None
        self.controller = None

    def tick(self, delta_time):
        pass

    def set_image(self, url, name):
        # sets player image (name must include file typr like '.png')
        self.image = pygame.image.load(os.path.join(url, name))

    def render_to_screen(self, window: pygame.display, camera: game_tools.my_camera):
        # this function should be invoked only by renderer.
        window.blit(self.image, camera.world_to_cam(self.x_pos, self.y_pos))
